package fr.poly.mtp.ig5.iwa.user.service;

import fr.poly.mtp.ig5.iwa.user.config.KeycloakSecurityConfig;
import fr.poly.mtp.ig5.iwa.user.entity.UserUpdate;
import fr.poly.mtp.ig5.iwa.user.exceptions.NoUserFoundException;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Collections;
import java.util.List;

@Service
public class UserService {

    private static final String TOPIC = "users";

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;



    private UsersResource usersResource = KeycloakSecurityConfig.getInstance().realm(KeycloakSecurityConfig.realm).users();

    public int update(String id, UserUpdate user) {
        UserRepresentation dbUser = this.usersResource.get(id).toRepresentation();
        UserResource userToSet = this.usersResource.get(id);
        dbUser.setFirstName(user.getFirstname());
        dbUser.setLastName(user.getLastname());
        userToSet.update(dbUser);
        return 0;
    }

    public int updatePassword(String id,UserUpdate user) {
        if (user.getNewPassword().equals(user.getConfirmPassword())) {
            UserResource userToSet = this.usersResource.get(id);
            UserRepresentation newUser = this.usersResource.get(id).toRepresentation();
            CredentialRepresentation passwordCredentials = new CredentialRepresentation();
            passwordCredentials.setTemporary(false);
            passwordCredentials.setType(CredentialRepresentation.PASSWORD);
            passwordCredentials.setValue(user.getNewPassword());
            CredentialRepresentation oldCredentials = this.usersResource.get(id).credentials().get(0);
            userToSet.removeCredential(oldCredentials.getId());
            newUser.setCredentials(Collections.singletonList(passwordCredentials));
            userToSet.update(newUser);
            return 0;
        } else {
            return 1;
        }

    }


}
