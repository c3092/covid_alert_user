package fr.poly.mtp.ig5.iwa.user.exceptions;

public class NoUserFoundException extends RuntimeException{

    public NoUserFoundException(String  id){
        super("No user found for this id: " + id);
    }


}
