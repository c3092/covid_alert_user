package fr.poly.mtp.ig5.iwa.user.controller;



import fr.poly.mtp.ig5.iwa.user.entity.UserUpdate;
import fr.poly.mtp.ig5.iwa.user.exceptions.NoUserFoundException;
import fr.poly.mtp.ig5.iwa.user.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.security.Principal;
import java.util.List;

@RestController
@RolesAllowed("user")
@RequestMapping("/users")
public class UserController {

    private final UserService userService;


    UserController(UserService userService) {
        this.userService = userService;
    }

    @PutMapping("{id}")
    public int update(@PathVariable String id, @RequestBody UserUpdate userUpdate){
        int code = 2;
        if (userUpdate.getFirstname() != null && userUpdate.getLastname() != null) {
             code =  userService.update(id,userUpdate);
        }
        if (userUpdate.getNewPassword() != null) {
            code = userService.updatePassword(id,userUpdate);
        }
        return code;
    }
}
